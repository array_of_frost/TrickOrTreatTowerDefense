Trick Or Treat Tower Defense.

A cute little game I started working on for Apptober, that I intend to finish
some day. Hopefully before next Apptober!

Fend off trick-or-treaters by throwing candy at them until they go away. If you can't fill their candy bags before they get to your porch, they'll egg your house!


