﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleProjectile : MonoBehaviour {
	
	public float speed;
	public int sugar = 1;
	// Use this for initialization
	void Start () {
		
	}

	public void aimAtTarget(GameObject obj) {
		aimAtTarget(obj.transform);
	}

	public void aimAtTarget(Transform target) {
		Vector2 direction = (Vector2)(target.position - transform.position);
		aimDirection(direction);
	}

	public void aimDirection(Vector2 direction) {
		GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
	}

	void OnCollisionEnter2D(Collision2D other) {
		print("Collided with " + other.gameObject.name);
		if (other.gameObject.tag == "baddy") {	
			print("Hit baddy!");
			other.gameObject.SendMessage("takeDamage", sugar);
			Destroy(gameObject);
		}
	}

	void OnBecameInvisible() {
		Destroy(gameObject);
	}
}

