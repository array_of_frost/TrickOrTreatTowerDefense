﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnackOLantern : MonoBehaviour {
	public GameObject projectile;
	public float fire_rate;

	Transform fire_point;
	float timestamp;
	Animator anim;
	List<Collider2D> targets;
	int spitHash = Animator.StringToHash("PumpkinSpit");

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		targets = new List<Collider2D>();
		timestamp = Time.time;
		fire_point = transform.Find("fire_point");
	}
	
	// Update is called once per frame
	void Update () {
		if (targets.Count > 0) {
			if (timestamp < Time.time) {
				// Fire!
				anim.SetTrigger(spitHash);
				GameObject proj = Instantiate(projectile);
				proj.transform.position = fire_point.position;
				SimpleProjectile sp = proj.GetComponent<SimpleProjectile>();
				sp.aimAtTarget(targets[0].gameObject);
				timestamp = Time.time + fire_rate;
			}
		}

	}
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "baddy") {
			targets.Add(other);
		}

	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.tag == "baddy") {
			targets.Remove(other);
		}
	}

}
