﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrickOrTreater : MonoBehaviour {
	public Transform destination;
	public int capacity; 
	public float speed; 
	public BaddyManager manager; 
	public static int sprite_number = 0;
	// Use this for initialization
	private SpriteRenderer sr;
	void Start () {
		sr = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		sr.sortingOrder = (int)(gameObject.transform.position.y*-1000);
		float step = Time.deltaTime * speed;
		transform.position = Vector3.MoveTowards(transform.position, destination.position, step);
		if (transform.position == destination.position) {
			manager.reachedDestination(gameObject);
		}
	}

	public void takeDamage(int amount) {
		print("Taking damage");
		capacity -= amount;
		if (capacity <= 0) {
			Destroy(gameObject);
		}
	}
		
//
//	void OnTriggerEnter2D(Collider2D other) {
//		print("Collided with " + other.gameObject.name);
//		if (other.gameObject.tag == "projectile") {	
//			Destroy(gameObject);
//		}
//	}
}
