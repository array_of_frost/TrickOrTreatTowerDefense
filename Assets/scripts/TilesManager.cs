﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum T {
	R,G
};

/*public enum T {
	CBR, SBE, BBL, G01,
	SRE, BTL, BTR, G02,
	STE, SLE, BBR, G03,
	CTR, CTL, CBL, G04
};
*/

public class TilesManager : MonoBehaviour {
	public GameObject[] tiles;
	private T[,] levelMap_1 = {
		{T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.R, T.R, T.R, T.R, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.R, T.G, T.G, T.R, T.G, T.G, T.G},
		{T.R, T.R, T.R, T.R, T.G, T.R, T.G, T.G, T.R, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.R, T.G, T.R, T.G, T.G, T.R, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.R, T.G, T.R, T.G, T.G, T.R, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.R, T.G, T.R, T.G, T.G, T.R, T.R, T.R, T.R},
		{T.G, T.G, T.G, T.R, T.R, T.R, T.G, T.G, T.G, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G},
		{T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G, T.G}
	};
	/*private T[,] levelMap_1 = {
		{T.G02, T.G03, T.G01, T.G04, T.CBR, T.SBE, T.SBE, T.SBE, T.SBE, T.CBL, T.G01},
		{T.G01, T.G02, T.G03, T.G01, T.SRE, T.BTL, T.STE, T.STE, T.BTR, T.SLE, T.G02},
	  	{T.G03, T.G04, T.G02, T.G04, T.SRE, T.SLE, T.G03, T.G01, T.SRE, T.SLE, T.G03},
	    {T.SBE, T.SBE, T.SBE, T.CBL, T.SRE, T.SLE, T.G01, T.G04, T.SRE, T.SLE, T.G04},
	    {T.STE, T.STE, T.BTR, T.SLE, T.SRE, T.SLE, T.G02, T.G03, T.SRE, T.BBL, T.SBE},
	    {T.G04, T.G01, T.SRE, T.SLE, T.SRE, T.SLE, T.G01, T.G02, T.CTR, T.STE, T.STE},
	    {T.G03, T.G02, T.SRE, T.BBL, T.BBR, T.SLE, T.G04, T.G03, T.G01, T.G02, T.G03},
	    {T.G01, T.G04, T.CTR, T.STE, T.STE, T.CTL, T.G01, T.G02, T.G03, T.G04, T.G01},
		{T.G02, T.G03, T.G01, T.G04, T.G02, T.G01, T.G04, T.G03, T.G01, T.G02, T.G03}};
	 */

	void Start () {
		
		Transform tilesParent = gameObject.transform.Find("LevelTiles");
		for (int row =0; row < levelMap_1.GetLength(0); row++) {
			for (int col = 0; col < levelMap_1.GetLength(1); col++ ) {
				T tile = levelMap_1[row,col];
				GameObject orig = tiles[(int)tile];

				GameObject obj = Instantiate(orig, tilesParent);
				obj.transform.localPosition = new Vector3(col * 1f, row * -1f);
			}
		}

	}








}