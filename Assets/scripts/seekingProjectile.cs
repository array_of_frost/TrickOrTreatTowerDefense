﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class seekingProjectile : MonoBehaviour {
	public GameObject target;
	public float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( target == null) {
			Destroy(gameObject);
		} else {
		float step = speed * Time.deltaTime;
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, step);
		}
	}
		
}
