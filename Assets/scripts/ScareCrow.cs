﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScareCrow : MonoBehaviour {
	Animator anim;
	int throwHash = Animator.StringToHash("ScareCrowThrow");
	public GameObject projectile;
	public float fire_rate;
	public int num_projectiles;
	Transform fire_point; 
	float timestamp;
	List<Collider2D> targets;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		targets = new List<Collider2D>();
		timestamp = Time.time;
		fire_point = transform.Find("fire_point");
	}


	// Update is called once per frame
	void Update () {
		if (targets.Count > 0) {
			if (timestamp < Time.time) {
				// Fire!
				anim.SetTrigger(throwHash);
				for (int i = 0; i < num_projectiles; i ++) {
					Quaternion degree_rotation = Quaternion.AngleAxis((i*60.0f/num_projectiles - 30f), Vector3.forward);
					print(1*45.0f/num_projectiles);
					Vector3 direction = targets[0].gameObject.transform.position - transform.position;
					direction = degree_rotation * direction;
					GameObject proj = Instantiate(projectile);
					proj.transform.position = fire_point.position;
					proj.transform.rotation = Quaternion.FromToRotation(Vector3.up, direction);
					SimpleProjectile sp = proj.GetComponent<SimpleProjectile>();
					sp.aimDirection((Vector2)direction);
				}
				timestamp = Time.time + fire_rate;
			}
		}

	}


	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "baddy") {
			targets.Add(other);
		}

	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.tag == "baddy") {
			targets.Remove(other);
		}
	}
}
