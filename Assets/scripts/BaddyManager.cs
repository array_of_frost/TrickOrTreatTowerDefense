﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaddyManager : MonoBehaviour {

	public GameObject baddy;
	public Transform[] paths;
	public List<GameObject> baddies;
	public int num_baddies;
	public float baddy_frequency;
	private int baddies_spawned;
	private float next_spawn_time;
	// Use this for initialization
	void Start () {
		baddies_spawned = 0;
		next_spawn_time = Time.time + baddy_frequency;
	}
	
	// Update is called once per frame
	void Update () {
		if (baddies_spawned < num_baddies) {
			if (Time.time >= next_spawn_time) {
				spawn();
			}
		}
	}

	void spawn() {
		GameObject b = Instantiate(baddy, paths[0].position, paths[0].rotation); 
		TrickOrTreater t = b.GetComponent<TrickOrTreater>();
		t.manager = this;
		t.destination = paths[1];
		baddies.Add(b);
		baddies_spawned++;
		next_spawn_time = Time.time + baddy_frequency;
	}

	public void reachedDestination(GameObject baddy) {
		TrickOrTreater t = baddy.GetComponent<TrickOrTreater>();
		for (int x = 0; x < paths.Length; x++ ) {
			if (paths[x] == t.destination) {
				// check if at end
				if (x == paths.Length-1) {
					// Take damage or something!
					print("Reached the END!");
					baddies.Remove(baddy);
					Destroy(baddy);
				} else {
					// set next destination
					t.destination = paths[x+1];
				}
				break;
			}
		}
	}


}
